define( [ "jquery", "jquery.artDialog" ], function($) {
	var index = {};
	index.search = function() {
		if ($("#search_key").val() == '') {
			return false;
		}
		$("#search_form").attr("action",
				"1-0-0-0-" + encodeURI(encodeURI($("#search_key").val()))+".html");
		$("#search_form").submit();
	}
	index.category = function() {
		var cate = $(this).attr("ref");
		$(this).attr("href", "cates/n4-1-" + encodeURI(encodeURI(cate)));
	}
	var events;
	index.qrcode = {
		mouseenter : function() {
			//设定程序延迟一秒钟执行
			events = setTimeout(function(){
				$.dialog( {
					lock : true,
					background : '#000000',
					opacity : 0.7,
					fixed : true,
					drag : false,
					resize : false,
					cancel : false,
					title : '',
					content : document.getElementById("qrcode_detail")
				});
			}, 1000); 
		},
		mouseleave : function(){
			clearTimeout(events);
		}
	}
	$("#qrcode_close").click(function(){
		var list = $.dialog.list;
		for (var i in list) {
		    list[i].close();
		};
	});
	return index;
});