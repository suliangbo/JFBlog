define( [ "jquery", "jquery.artDialog" ], function($) {
	var tags = {};
	
	//添加标签
	tags.add = function(){
		var category = $("#tags_category").val();
		$("#tags_form").attr("action",$("#tags_form").attr("action")+category);
		$("#tags_form").submit();
	}
	
	//编辑标签
	tags.update = function(){
		var tagid = $(this).attr("tagid");
		var dialog = $.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '消息提示',
			button: [
		        {
		            name: '保存',
		            callback:function(){
		        		$.ajax({
						   type: "POST",
						   url: "admin/savetagedit",
						   data: $("#tagsedit_form").serialize(),
						   success: function(msg){
						   	  alert(msg);
						   	  window.location.href = window.location.href;
						   }
						});
		            }
		        },
		        {
		            name: '取消'
		        }
   			 ]
		});
		$.ajax({
			url : 'admin/tagedit/'+tagid,
			success: function (data) {
		        dialog.content(data);
		    },
		    cache: false
		});
	}
	
	//删除标签
	tags.remove = function(){
		var tagid = $(this).attr("tagid");
		$.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '消息提示',
			content:'删除操作会同步删除此标签下所有文章，确定删除吗？',
			button: [
		        {
		            name: '确定',
		            callback:function(){
		        		$.ajax({
						   type: "POST",
						   url: "admin/tagdelete/"+tagid,
						   success: function(msg){
						   	  alert(msg);
						   	  window.location.href = window.location.href;
						   }
						});
		            }
		        },
		        {
		            name: '取消'
		        }
   			 ]
		});
	}
	
	return tags;
});