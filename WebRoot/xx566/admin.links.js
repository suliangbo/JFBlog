define( [ "jquery", "jquery.artDialog" ], function($) {
	var links = {};
	
	//编辑友链
	links.update = function(){
		var linksid = $(this).attr("linksid");
		var dialog = $.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '消息提示',
			button: [
		        {
		            name: '保存',
		            callback:function(){
		        		$.ajax({
						   type: "POST",
						   url: "admin/savelinksedit.html",
						   data: $("#linksedit_form").serialize(),
						   success: function(msg){
						   	  alert(msg);
						   	  window.location.href = window.location.href;
						   }
						});
		            }
		        },
		        {
		            name: '取消'
		        }
   			 ]
		});
		$.ajax({
			url : 'admin/linksedit/'+linksid+".html",
			success: function (data) {
		        dialog.content(data);
		    },
		    cache: false
		});
	}
	
	//删除友链
	links.remove = function(){
		var linksid = $(this).attr("linksid");
		$.dialog({
    		lock: true,
    		background: '#000000', 
		    opacity: 0.5,
		    fixed: true,
			drag: false,
    		resize: false,
			title: '消息提示',
			content:'确定删除此友情链接吗？',
			button: [
		        {
		            name: '确定',
		            callback:function(){
		        		$.ajax({
						   type: "POST",
						   url: "admin/linksdelete/"+linksid+".html",
						   success: function(msg){
						   	  alert(msg);
						   	  window.location.href = window.location.href;
						   }
						});
		            }
		        },
		        {
		            name: '取消'
		        }
   			 ]
		});
	}
	
	return links;
});