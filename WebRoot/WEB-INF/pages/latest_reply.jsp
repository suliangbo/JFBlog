<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<article class="recent-comments">
	<h3>最近回复</h3>
	<ul>
		<c:forEach items="${messages.list}" var="msg">
			<li>
				<a rel="external nofollow" href="${msg.website}">${msg.name}</a> :
				<c:if test="${msg.articleid==-1}"><!-- 关于JFBlog留言 -->
					<a href="about/n1.html">${msg.content}</a>
				</c:if>
				<c:if test="${msg.articleid==-2}"><!-- 留言板 -->
					<a href="message/n2.html">${msg.content}</a>
				</c:if>
				<c:if test="${msg.articleid==-3}"><!-- 归档留言 -->
					<a href="archive/n3.html">${msg.content}</a>
				</c:if>
				<c:if test="${msg.articleid==-4}"><!-- category分类留言 -->
					<a href="cates/n4.html">${msg.content}</a>
				</c:if>
				<c:if test="${msg.articleid>0}"><!-- 文章留言 -->
					<a href="detail/${msg.articleid}.html">${msg.content}</a>
				</c:if>
	        </li>
		</c:forEach>
	</ul>
</article>
