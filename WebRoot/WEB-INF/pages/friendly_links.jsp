<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<article class="friend-list">
    <h3>友情链接</h3>
    <ul>
    	<c:forEach items="${friendlys}" var="f">
		    <li><a target="_blank" href="${f.url}">${f.name}</a></li>
    	</c:forEach>
    </ul>
</article>
