<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
	<%@include file="/common/common_meta.jsp" %>
	<link href="css/grid.css" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" rel="stylesheet" type="text/css" />
	<link href="css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link href="css/dialog.css" rel="stylesheet" type="text/css"/>
</head>
<body>
	<%@include file="../include/header.jsp" %>
	<section>
		<div class="main">
			<div class="body container">
				<div>
					<form action="admin/tagList/${page.num}-" method="post" id="tags_form">
						<select id="tags_category">
							<option value="ALL">全部</option>
							<c:forEach items="${tags_cateList}" var="cat">
								<option value="${cat.cate}" <c:if test="${cat.cate eq tag_category}">selected="selected"</c:if>>${cat.cate}</option>
							</c:forEach>
						</select>
						<input type="button" id="tags_btn" value="查询"/><span class="icon-archive"></span><a tagid="0" id="tags_update" title="新增标签" style="cursor: pointer;">新增标签 </a>
					</form>
				</div> 
				<div class="colgroup">
			    	<div class="typecho-page-title col-mb-12">
				        <table width="100%">
		                    <colgroup>
		                        <col width="25%">
		                        <col width="20%">
		                        <col width="10%">
		                        <col width="20%">
		                        <col width="10%">
		                        <col width="15%">
		                    </colgroup>
		                    <thead>
		                        <tr>
		                            <th>名称</th>
		                            <th>分类</th>
		                            <th>关注</th>
		                            <th>日期</th>
		                            <th>排序</th>
		                            <th>操作</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    <c:forEach items="${page.list}" var="tags">
		                    	<tr>
		                        	<td><a href="1-${tags.id}-0-0.html" target="_blank">${tags.name}</a></td>
		                        	<td align="center">${tags.type}</td>
		                        	<td align="center">${tags.counts}</td>
		                        	<td align="center"><fmt:formatDate value="${tags.createtime}" pattern="yyyy/MM/dd HH:mm" /></td>
		                        	<td align="center">${tags.orders}</td>
		                        	<td align="center"><a tagid="${tags.id}" id="tags_update" style="cursor: pointer;">编辑</a>|<a tagid="${tags.id}" id="tags_delete" style="cursor: pointer;">删除</a></td>
		                        </tr>
		                    </c:forEach>
		                    <c:if test="${empty page.list}">
		                    	<tr>
		                        	<td colspan="6"><h4 align="center">没有添加分类标签</h4></td>
		                        </tr>
							</c:if>
							<c:if test="${!empty page.list}">
								<tr align="right">
									<td colspan="6">
										<c:if test="${page.num!=1}">
											<a href="admin/tagList/1-a.html">首页</a>
											<a href="admin/tagList/${page.num-1}-${tag_category}.html">上一页</a>
										</c:if>
										<c:forEach var="i" begin="${page.begin}" end="${page.end}">
											<a <c:if test="${page.num==i}">style="font-size: 18px;"</c:if> href="admin/tagList/${i}-${tag_category}.html">${i}</a>
										</c:forEach>
										<c:if test="${page.navCount!=0&&page.num!=page.last}">
											<a href="admin/tagList/${page.num+1}-${tag_category}.html">下一页</a>
											<a href="admin/tagList/${page.navCount}-${tag_category}.html">尾页</a>
										</c:if>
									</td>
								</tr>
							</c:if>
		                    </tbody>
		                </table>
			    	</div>
				</div>
		   	</div>
		</div>
	</section>
	<script src="js/require.js" defer async="true" data-main="js/admin_tags"></script>
</body>
</html>