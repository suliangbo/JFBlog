<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
	<%@include file="/common/common_meta.jsp" %>
</head>
<body>
	<form id="tagsedit_form">
		<input type="hidden" name="tags.id" value="${tags_tags.id}"/>
		<div>
			<span>&nbsp;&nbsp;关注度：</span>
			<span><input type="text" name="tags.counts" value="${tags_tags.counts}"/></span>
		</div>
		<div>
			<span>&nbsp;&nbsp;排序号：</span>
			<span><input type="text" name="tags.orders" value="${tags_tags.orders}"/></span>
		</div>
		<div>
			<span>标签名称：</span>
			<span><input type="text" name="tags.name" value="${tags_tags.name}"/></span>
		</div>
		<div>
			<span>所属分类：</span>
			<select id="cates_select" onchange="javascript:$('#tags_type').val($(this).val());">
				<c:if test="${empty tags_cateList}"><option>尚未添加分类</option></c:if>
				<c:forEach items="${tags_cateList}" var="cat">
					<option value="${cat.cate}" <c:if test="${cat.cate eq tags_tags.type}">selected="selected"</c:if>>${cat.cate}</option>
				</c:forEach>
			</select>
		</div>
		<div>
			<span>手动输入：</span>
			<span>
				<c:if test="${empty tags_tags.type}">
					<input type="text" id="tags_type" name="tags.type" value="${tags_cateList[0].cate}"/>
				</c:if>
				<c:if test="${!empty tags_tags.type}">
					<input type="text" id="tags_type" name="tags.type" value="${tags_tags.type}"/>
				</c:if>
			</span>
		</div>
	</form>
	<script src="js/require.js" defer async="true" data-main="js/admin_tags"></script>
</body>
</html>


