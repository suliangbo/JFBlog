//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.artDialog" : "js/jquery.artDialog",
		"admin.article" : "xx566/admin.article"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//文章管理
require( [ "jquery", "admin.article" ], function($, article) {
	//创建索引
	$("#arts_indexes").click(article.createIndexes);
	//删除文章
	$("a[id^='art_delete']").click(article.deleteArticle);
});