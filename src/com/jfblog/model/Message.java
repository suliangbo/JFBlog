package com.jfblog.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * 
 * @ClassName: Message
 * @Description: 留言信息(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-6 下午05:16:52
 * 
 */
@SuppressWarnings("serial")
public class Message extends Model<Message> {

	public static final Message dao = new Message();

	/**
	 * 查询留言信息
	 * 
	 * @param pageNum
	 *            当前页码
	 * @param pageSize
	 *            每页条数
	 * @param pid
	 *            留言PID
	 * @param articleid
	 *            文章ID
	 * @param ispage
	 *            是否分页查询
	 * @param isdesc
	 *            是否倒序
	 * @return
	 */
	public Object findMsg4Page(Integer pageNum, Integer pageSize, Integer pid, Integer articleid, Boolean ispage,
			boolean isdesc) {
		String select = "select t1.id,t1.content,t1.createtime,t2.name,t2.avatar,t2.website ";
		String sqlExcept = "from message t1,user t2 where t1.userid=t2.id and t1.pid=? and t1.articleid=? order by t1.createtime "
				+ (isdesc ? "desc" : "asc");
		if (ispage) {
			return paginate(pageNum, pageSize, select, sqlExcept, new Object[] { pid, articleid });
		}
		return find(select + sqlExcept, new Object[] { pid, articleid });
	}

	/**
	 * 留言管理 获取所有留言信息
	 * 
	 * @Title: findMsg4Page
	 * @author Realfighter
	 * @param pagenum
	 * @param pageSize
	 * @return Page<Message>
	 * @throws
	 */
	public Page<Message> findMsg4Page(int pagenum, int pageSize) {
		Page<Message> page = paginate(pagenum, pageSize,
				"select t1.*,t2.name,t2.website,t2.avatar,t2.email,t2.userip ",
				"from message t1,user t2 where t1.userid=t2.id order by t1.createtime desc");
		return page;
	}

	/**
	 * 删除留言信息
	 * 
	 * @Title: deleteByMsgid
	 * @author Realfighter
	 * @param msgid
	 *            void
	 * @throws
	 */
	public void deleteByMsgid(int msgid) {
		List<Message> msgs = find("select id from message where pid=?", msgid);
		for (Message msg : msgs) {
			deleteById(msg.getInt("id"));
		}
	}
}
