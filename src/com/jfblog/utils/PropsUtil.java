package com.jfblog.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @ClassName: PropsUtil
 * @Description: 配置文件读取(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-6 下午04:26:32
 * 
 */
public class PropsUtil {

	static Properties props = new Properties();

	static {
		try {
			props.load(PropsUtil.class.getClassLoader().getResourceAsStream(
					"config.properties"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static String get(String key) {
		return props.getProperty(key);
	}
}
